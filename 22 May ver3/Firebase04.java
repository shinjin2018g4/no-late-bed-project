import java.io.FileInputStream;
import java.util.Date;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.tasks.OnCompleteListener;
import com.google.firebase.tasks.Task;

public class Firebase04 {

	  static String JSON_FILE_NAME = "C:\\Users\\ctctime24\\Downloads\\no-late bed project-8ae39642d037.json"; // 認証情報の JSONファイル名のパスを指定。
	  static String URL = "https://testv2-77226.firebaseio.com/"; // Firebase上のリアルタイムデータベースのURLを指定。

	  // Firebase にデータを送信

	  public static void First() throws Exception{

		  // 初期化。

		  FileInputStream serviceAccount = new FileInputStream(JSON_FILE_NAME);

		  FirebaseOptions options = new FirebaseOptions.Builder()
		        .setCredentials(GoogleCredentials.fromStream(serviceAccount))
		        .setDatabaseUrl(URL)
		        .build();
		  FirebaseApp.initializeApp(options);
	  }

	  public static void SendingDataToFirebase(int sign) throws Exception
	  {
		  System.out.println("メソッドの処理が開始されました。");

		  DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
	      OnCompleteListener listener = new OnCompleteListener()
	     {
	      @Override
	      public void onComplete(Task task)
	      {
	        System.out.println("送信完了");
	      }
	     };

		    System.out.println("メソッドの処理が終了しました。");

	    reference.child("message").setValue(sign).addOnCompleteListener(listener);
	    reference.child("time").push().setValue("送信時刻 " + new Date()).addOnCompleteListener(listener);
	  }
}