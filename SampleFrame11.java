import java.awt.BorderLayout;
import java.io.FileInputStream;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class SampleFrame11 extends JFrame
{
  static String JSON_FILE_NAME = "C:\\Users\\ctctime24\\Downloads\\test-1e7a4e4fb272.json"; // 認証情報の JSONファイル名のパスを指定。
  static String URL = "https://test-39a24.firebaseio.com/"; // Firebase上のリアルタイムデータベースのURLを指定。

  private String[] column_names = {"処理", "キー", "値"};

  DefaultTableModel table_model = new DefaultTableModel(column_names , 0);
  JTable table_view = new JTable(this.table_model);

  public SampleFrame11()
  {
    this.setTitle("Sample No.11");
    this.setSize(800, 400);

    JScrollPane scroll = new JScrollPane(this.table_view);

    this.setLayout(new BorderLayout());
    this.add(scroll, BorderLayout.CENTER);

    // Firebase初期化。

    try
    {
      FileInputStream serviceAccount = new FileInputStream(JSON_FILE_NAME);

      FirebaseOptions options = new FirebaseOptions.Builder()
          .setCredentials(GoogleCredentials.fromStream(serviceAccount))
          .setDatabaseUrl(URL)
          .build();

      FirebaseApp.initializeApp(options);
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }

    // Firebase の「ルート」の監視をするリスナーを生成。

    ChildEventListener listener = new ChildEventListener()
    {
      @Override
      public void onChildAdded(DataSnapshot snapshot, String name)
      {
        Object data[] = new Object[3];
        data[0] = snapshot.getKey();
        data[1] = snapshot.getValue();
        data[2] = "追加されました。";
        table_model.addRow(data);
      }

      @Override
      public void onChildChanged(DataSnapshot snapshot, String name)
      {
        Object data[] = new Object[3];
        data[0] = snapshot.getKey();
        data[1] = snapshot.getValue();
        data[2] = "変更されました。";
        table_model.addRow(data);
      }

      @Override
      public void onChildMoved(DataSnapshot snapshot, String name)
      {
        Object data[] = new Object[3];
        data[0] = snapshot.getKey();
        data[1] = snapshot.getValue();
        data[2] = "移動されました。";
        table_model.addRow(data);
      }

      @Override
      public void onChildRemoved(DataSnapshot snapshot)
      {
        Object data[] = new Object[3];
        data[0] = snapshot.getKey();
        data[1] = snapshot.getValue();
        data[2] = "削除されました。";
        table_model.addRow(data);
      }

      public void onCancelled1(DatabaseError error)
      {
        System.out.println("通信がキャンセルされました。");
      }

      @Override
      public void onCancelled(DatabaseError arg0)
      {
        // TODO 自動生成されたメソッド・スタブ

      }
    };

    // Firebase の「ルート」の監視を開始。

    DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
    reference.child("folder").addChildEventListener(listener);
    reference.child("time").addChildEventListener(listener);
  }
}

