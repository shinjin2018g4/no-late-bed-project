import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class Station9 extends JFrame implements ActionListener{

  JComboBox combo1;
  JComboBox combo1_1;
  JComboBox combo2;
  JComboBox combo2_1;
  JLabel label;

  public static void main(String[] args){
	  Station9 frame = new Station9();

    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setSize(240, 230);
    frame.setLocationRelativeTo(null);
    frame.setTitle("タイトル");
    frame.setVisible(true);
  }

  Station9(){
    String[] startTime = {"00","01","02","03",
            "04","05","06","07",
            "08","09","10","11",
            "12","13","14","15",
            "16","17","18","19",
            "20","21","22","23"};
    String[] startTime2 = {"00","05","10","15",
           	"20","25","30","35",
            "40","45","50","55"};

    String[] endTime =  {"00","01","02","03",
            "04","05","06","07",
            "08","09","10","11",
            "12","13","14","15",
            "16","17","18","19",
            "20","21","22","23"};

     String[] endTime2 =   {"00","05","10","15",
               "20","25","30","35",
               "40","45","50","55"};



    combo1 = new JComboBox(startTime);
    combo1.setPreferredSize(new Dimension(50, 30));

    combo1.addActionListener(this);

    combo1_1 = new JComboBox(startTime2);
    combo1_1.setPreferredSize(new Dimension(50, 30));

    combo1_1.addActionListener(this);

    combo2 = new JComboBox(endTime);
    combo2.setPreferredSize(new Dimension(50, 30));
    combo2.setMaximumRowCount(5);
    combo2.addActionListener(this);

    combo2_1 = new JComboBox(endTime2);
    combo2_1.setPreferredSize(new Dimension(50, 30));
    combo2_1.addActionListener(this);

    JButton button1 = new JButton("Send");
    button1.addActionListener(this);


    JPanel p = new JPanel();
    p.setPreferredSize(new Dimension(100, 60));
    FlowLayout layout = new FlowLayout();
    layout.setAlignment(FlowLayout.LEFT);
    p.setLayout(layout);
    p.add(new JLabel("  Arrival time:        "));
    p.add(combo1);
    p.add(combo1_1);

    JPanel q = new JPanel();
    q.setPreferredSize(new Dimension(70, 80));
    FlowLayout layout2 = new FlowLayout();
    layout2.setAlignment(FlowLayout.LEFT);
    q.setLayout(layout2);
    q.add(new JLabel("  Ready time:        "));
    q.add(combo2);
    q.add(combo2_1);

    JPanel r = new JPanel();
    r.setPreferredSize(new Dimension(70, 80));
    FlowLayout layout3 = new FlowLayout();
    layout3.setAlignment(FlowLayout.LEFT);
    r.setLayout(layout3);
    JTextField destination1 = new JTextField(12);
    r.add(new JLabel("Arrival Location:"));
    r.add(destination1);
    r.add(button1);

    label = new JLabel();
    JPanel labelPanel = new JPanel();
    labelPanel.add(label);

    getContentPane().add(p, BorderLayout.NORTH);
    getContentPane().add(q, BorderLayout.CENTER);
    getContentPane().add(r, BorderLayout.SOUTH);
    getContentPane().add(labelPanel, BorderLayout.EAST);
  }

  public void actionPerformed(ActionEvent e) {
    String arrivaltime1;
    String arrivaltime2;
    String readytime1;
    String readytime2;


    if (combo1.getSelectedIndex() == -1){
        arrivaltime1 = "(not select)";
    }else{
        arrivaltime1 = (String)combo1.getSelectedItem();
    }

    if (combo1_1.getSelectedIndex() == -1){
        arrivaltime2 = "(not select)";
    }else{
        arrivaltime2 = (String)combo1_1.getSelectedItem();
    }
    if (combo1_1.getSelectedIndex() == -1){
        readytime1 = "(not select)";
    }else{
        readytime1 = (String)combo2.getSelectedItem();
    }
    if (combo1_1.getSelectedIndex() == -1){
        readytime2 = "(not select)";
    }else{
        readytime2 = (String)combo2_1.getSelectedItem();
    }


    label.setText("Arrival Time:" + arrivaltime1 + ":" + arrivaltime2 + ", Ready Time:" + readytime1 + ":" + readytime2 );
  }
}