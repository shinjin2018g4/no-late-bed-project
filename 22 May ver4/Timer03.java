import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class Timer03 {


	public static long durationTime;
	public static long durationTime_old;
	public static long arrivalTime;
	public static long readyTime;
	public static long alarmTime;
	public static long departureTime;
	public static int stopalarm;
	public static int sign;
	public static int sign2;
	public static int sign3;

	public static  Firebase04 firebase = new Firebase04();

	public void getDTime(String origin, String destination, long arrivalTimeMili) throws IOException {

		URL url = new URL( "https://maps.googleapis.com/maps/api/directions/json?"
        		+ "origin="+ origin +"&destination="+ destination +"&"
    			+ "arrival_time=" + arrivalTimeMili
        		+ "&mode=transit&transit_mode=rail&"
        		+ "key=AIzaSyAg0eO43cWO7onOVkiEjDPR1vJl6OJ6RNU");

		HttpURLConnection httpConn = (HttpURLConnection) url.openConnection();
		InputStream in = httpConn.getInputStream();
		BufferedReader r = new BufferedReader(
		new InputStreamReader(in,"EUC-JP"));

//		for (;;) {
//			String line = r.readLine();
//
//			if (line.matches(".*duration.*")) {
//				line = r.readLine();
//				line = r.readLine();
//				int idx = line.indexOf(":");
//			durationTime = Integer.parseInt(line.substring(idx+2));
//			break;
//			}
//		}
		durationTime = 600;

	}

	public static void setATime(long arrivalTime, long readyTime, long durationTime) {

		alarmTime = arrivalTime - readyTime - durationTime;

	}

	public void setDTime(long arrivalTime, long durationTime) {

		departureTime = arrivalTime - durationTime;

	}

	public void speakATime() {

		BouyomiChan4J bouyomi = new BouyomiChan4J();

		SimpleDateFormat dfmt = new SimpleDateFormat("h時m分");
		long ctime = System.currentTimeMillis();
		long drt_m = (durationTime % 3600) / 60;
		long drt_h = durationTime / 3600;

		Date strct = new Date(ctime);
		Date strdptt = new Date(departureTime*1000);
		String strdrt;
		if (drt_h == 0) {
			strdrt = String.format("%d",drt_m) + "分" ;
		}else {
			strdrt = String.format("%d",drt_h) + "時間" + String.format("%d",drt_m) + "分" ;
		}

		String spctime = "現在、"+ dfmt.format(strct) + "でございます。";
		String spdpttime = "出発予定時刻は"+ dfmt.format(strdptt) + "でございます。";
		String spdrtime = "移動時間は"+ strdrt + "でございます。";

		bouyomi.talk(-1,-1,110,2,spctime);
		bouyomi.talk(-1,-1,110,2,spdpttime);
		bouyomi.talk(-1,-1,110,2,spdrtime);

	}

	public void speakDTime() {

		BouyomiChan4J bouyomi = new BouyomiChan4J();

		SimpleDateFormat dfmt = new SimpleDateFormat("h時mm分");
		Date strdptt = new Date(departureTime*1000);
		String spdpttime = "出発予定時刻、"+ dfmt.format(strdptt) + "になりました。いってらっしゃい。";

		bouyomi.talk(-1,-1,110,2,spdpttime);

	}

	public void speakDTimeChange() {

		BouyomiChan4J bouyomi = new BouyomiChan4J();

		SimpleDateFormat dfmt = new SimpleDateFormat("h時mm分");
		Date strdptt = new Date(departureTime*1000);
		String spdpttime = "出発予定時刻が"+ dfmt.format(strdptt) + "に変更されました。";

		bouyomi.talk(-1,-1,110,2,spdpttime);

	}

	public void loopsetTime(String org, String dst, long tp) throws Exception {

		firebase.First();
		sign = 0;
		firebase.SendingDataToFirebase(sign);
		stopalarm = 0;

		Timer timer = new Timer();
		TimerTask tack = new TimerTask(){

			public void run(){

				try {
					durationTime_old = durationTime;
					getDTime(org,dst,tp);
					long currentTime = System.currentTimeMillis() / 1000;
					long diff = durationTime_old - durationTime;

					if (Math.abs(diff) > 0){
						if (currentTime<alarmTime) {
							setATime(arrivalTime,readyTime,durationTime);
							setDTime(arrivalTime,durationTime);
						}else {
							setDTime(arrivalTime,durationTime);
							speakDTimeChange();
						}
					}

					System.out.println(" ");
					System.out.println(alarmTime-currentTime);

					if ((departureTime-currentTime)<0){
						speakDTime();
						timer.cancel();

						Thread.sleep(2000);

						sign3 = 0;
						firebase.SendingDataToFirebase(sign3);
					}
					else if ((alarmTime-currentTime)<0 && stopalarm == 0){
						stopalarm = 100;
						sign2 = 100;
						firebase.SendingDataToFirebase(sign2);

						Thread.sleep(20000);

						speakATime();
					}

				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		};
		timer.schedule(tack,1000,1000);
	}
}