import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SpringLayout;

public class Station07 extends JFrame implements ActionListener{

	  JComboBox combo0;
	  JComboBox combo0_1;
	  JComboBox combo0_2;
	  JComboBox combo1;
	  JComboBox combo1_1;
	  JComboBox combo2;
	  JComboBox combo2_1;
	  JLabel label;
	  JTextField origin_text;
	  JTextField destination_text;
	  String date1;
	  String date2;
	  String date3;
	  String arrivaltime1;
	  String arrivaltime2;
	  String readytime1;
	  String readytime2;
		long time;
		long drtime;
		long altime;
		long dpttime;
		long timePercent;
		long readytimeSec;
		int sign;
	  Timer03 timer = new Timer03();
	  Firebase04 firebase = new Firebase04();


	  Station07(){

		String[] month = {"05",  "06"};
		String[] day = {"21", "22", "23", "24",
				"25", "26", "27", "28",
				"29", "30", "31"};

		String[] year = {"2018", "2019"};
//		String[] month = {"01", "02", "03", "04",
//				"05", "06", "07","08",
//				"09", "10", "11", "12"};
//		String[] day = {"01", "02", "03", "04",
//				"05", "06", "07", "08",
//				"09", "10","11", "12",
//				"13", "14", "15", "16",
//				"17", "18", "19", "20",
//				"21", "22", "23", "24",
//				"25", "26", "27", "28",
//				"29", "30", "31"};

	    String[] startTime = {"00","01","02","03",
	            "04","05","06","07",
	            "08","09","10","11",
	            "12","13","14","15",
	            "16","17","18","19",
	            "20","21","22","23"};
	    String[] startTime2 = {"00", "01", "02", "03", "04",
				"05", "06", "07", "08",
				"09", "10","11", "12",
				"13", "14", "15", "16",
				"17", "18", "19", "20",
				"21", "22", "23", "24",
				"25", "26", "27", "28",
				"29", "30", "31", "32",
				"33", "34", "35", "36",
				"37", "38", "39", "40",
				"41", "42", "43", "44",
				"45", "46", "47", "48",
				"49", "50", "51", "52",
				"53", "54", "55", "56",
				"57", "58", "59"};

	    String[] endTime =  {"00","01","02","03",
	            "04","05","06","07",
	            "08","09","10","11",
	            "12","13","14","15",
	            "16","17","18","19",
	            "20","21","22","23"};

	     String[] endTime2 =   {"00", "01", "02", "03", "04",
	 			"05", "06", "07", "08",
	 			"09", "10","11",  "12",
	 			"13", "14", "15", "16",
	 			"17", "18", "19", "20",
	 			"21", "22", "23", "24",
	 			"25", "26", "27", "28",
	 			"29", "30", "31", "32",
	 			"33", "34", "35", "36",
	 			"37", "38", "39", "40",
				"41", "42", "43", "44",
	 			"45", "46", "47", "48",
	 			"49", "50", "51", "52",
	 			"53", "54", "55", "56",
	 			"57", "58", "59"};


	    combo0 = new JComboBox(year);
	    combo0.setPreferredSize(new Dimension(55, 30));
	    combo0_1 = new JComboBox(month);
	    combo0_1.setPreferredSize(new Dimension(45, 30));
	    combo0_2 = new JComboBox(day);
	    combo0_2.setPreferredSize(new Dimension(45, 30));
	    combo1 = new JComboBox(startTime);
	    combo1.setMaximumRowCount(12);
	    combo1.setPreferredSize(new Dimension(50, 30));
	    combo1_1 = new JComboBox(startTime2);
	    combo1_1.setMaximumRowCount(15);
	    combo1_1.setPreferredSize(new Dimension(50, 30));
	    combo2 = new JComboBox(endTime);
	    combo2.setPreferredSize(new Dimension(50, 30));
	    combo2.setMaximumRowCount(5);
	    combo2_1 = new JComboBox(endTime2);
	    combo2_1.setPreferredSize(new Dimension(50, 30));

	    JButton button1 = new JButton("Send");
	    button1.addActionListener(this);

	    JPanel p = new JPanel();
	    p.setPreferredSize(new Dimension(240, 280));
	    SpringLayout layout = new SpringLayout();
	    p.setLayout(layout);

	    JLabel label0 = new JLabel("   Date:");
	    JLabel label1 = new JLabel("  Arrival time:        ");
	    JLabel label2 = new JLabel("  Ready time:        ");
	    JLabel label3 = new JLabel("Origin:");
	    JLabel label4 = new JLabel("Destination:");
	    origin_text = new JTextField("ヤンキースタジアム", 12);
	    destination_text = new JTextField("セントラルパーク動物園",12);

	    p.add(label0);
	    p.add(label1);
	    p.add(label2);
	    p.add(label3);
	    p.add(label4);
	    p.add(combo0);
	    p.add(combo0_1);
	    p.add(combo0_2);
	    p.add(combo1);
	    p.add(combo1_1);
	    p.add(combo2);
	    p.add(combo2_1);
	    p.add(origin_text);
	    p.add(destination_text);
	    p.add(button1);

	    layout.putConstraint(SpringLayout.NORTH, combo0, 10 ,SpringLayout.NORTH, p);
	    layout.putConstraint(SpringLayout.WEST, combo0, 50 ,SpringLayout.WEST, label0);
	    layout.putConstraint(SpringLayout.NORTH, combo0_1, 10 ,SpringLayout.NORTH, p);
	    layout.putConstraint(SpringLayout.WEST, combo0_1, 10 ,SpringLayout.EAST, combo0);
	    layout.putConstraint(SpringLayout.NORTH, combo0_2, 10 ,SpringLayout.NORTH, p);
	    layout.putConstraint(SpringLayout.WEST, combo0_2, 10 ,SpringLayout.EAST, combo0_1);
	    layout.putConstraint(SpringLayout.NORTH, combo1, 10, SpringLayout.SOUTH, combo0);
	    layout.putConstraint(SpringLayout.WEST, combo1, 85, SpringLayout.WEST, label1);
	    layout.putConstraint(SpringLayout.NORTH, combo1_1, 10, SpringLayout.SOUTH, combo0);
	    layout.putConstraint(SpringLayout.WEST, combo1_1, 20, SpringLayout.EAST, combo1);
	    layout.putConstraint(SpringLayout.NORTH, combo2, 10, SpringLayout.SOUTH, combo1);
	    layout.putConstraint(SpringLayout.WEST, combo2, 0, SpringLayout.WEST, combo1);
	    layout.putConstraint(SpringLayout.NORTH, combo2_1, 10, SpringLayout.SOUTH, combo1_1);
	    layout.putConstraint(SpringLayout.WEST, combo2_1, 0, SpringLayout.WEST, combo1_1);
	    layout.putConstraint(SpringLayout.NORTH, label0, 15, SpringLayout.NORTH, p);
	    layout.putConstraint(SpringLayout.WEST, label0, 2, SpringLayout.WEST, p);
	    layout.putConstraint(SpringLayout.NORTH, label1, 17, SpringLayout.SOUTH, label0);
	    layout.putConstraint(SpringLayout.WEST, label1, 2, SpringLayout.WEST, label0);
	    layout.putConstraint(SpringLayout.NORTH, label2, 20, SpringLayout.SOUTH, label1);
	    layout.putConstraint(SpringLayout.WEST, label2, 0, SpringLayout.WEST, label1);
	    layout.putConstraint(SpringLayout.NORTH, label3, 10, SpringLayout.SOUTH, label2);
	    layout.putConstraint(SpringLayout.WEST, label3, 5, SpringLayout.WEST, label2);
	    layout.putConstraint(SpringLayout.NORTH, label4, 10, SpringLayout.SOUTH, origin_text);
	    layout.putConstraint(SpringLayout.WEST, label4, 00, SpringLayout.WEST, origin_text);
	    layout.putConstraint(SpringLayout.NORTH, origin_text, 0, SpringLayout.SOUTH, label3);
	    layout.putConstraint(SpringLayout.WEST, origin_text, 0, SpringLayout.WEST, label3);
	    layout.putConstraint(SpringLayout.NORTH, destination_text, 0, SpringLayout.SOUTH, label4);
	    layout.putConstraint(SpringLayout.WEST, destination_text, 0, SpringLayout.WEST, label4);
	    layout.putConstraint(SpringLayout.NORTH, button1, -4, SpringLayout.NORTH, destination_text);
	    layout.putConstraint(SpringLayout.WEST, button1, 10, SpringLayout.EAST, destination_text);



	    label = new JLabel();  // 入力データを下に表示させる
	    JPanel labelPanel = new JPanel();
	    labelPanel.add(label);

	    layout.putConstraint(SpringLayout.NORTH, label, -30, SpringLayout.SOUTH, destination_text);
	    layout.putConstraint(SpringLayout.WEST, label, 0, SpringLayout.WEST, destination_text);


	    getContentPane().add(p, BorderLayout.NORTH);
	    getContentPane().add(labelPanel, BorderLayout.EAST);

	  }

	  public void actionPerformed(ActionEvent event) {



		if (combo0.getSelectedIndex() == -1){
		        date1 = "(not select)";
		}else{
		        date1 = (String)combo0.getSelectedItem();
		}

		if (combo0_1.getSelectedIndex() == -1){
	        date2 = "(not select)";
	    }else{
	        date2 = (String)combo0_1.getSelectedItem();
	    }

		if (combo0_2.getSelectedIndex() == -1){
	        date3 = "(not select)";
	    }else{
	        date3 = (String)combo0_2.getSelectedItem();
	    }

	    if (combo1.getSelectedIndex() == -1){
	        arrivaltime1 = "(not select)";
	    }else{
	        arrivaltime1 = (String)combo1.getSelectedItem();
	    }

	    if (combo1_1.getSelectedIndex() == -1){
	        arrivaltime2 = "(not select)";
	    }else{
	        arrivaltime2 = (String)combo1_1.getSelectedItem();
	    }
	    if (combo1_1.getSelectedIndex() == -1){
	        readytime1 = "(not select)";
	    }else{
	        readytime1 = (String)combo2.getSelectedItem();
	    }
	    if (combo1_1.getSelectedIndex() == -1){
	        readytime2 = "(not select)";
	    }else{
	        readytime2 = (String)combo2_1.getSelectedItem();
	    }

	    
	    String str = date1 + date2 + date3 + " " +
	    		arrivaltime1 + arrivaltime2;
	    System.out.println(str);
	    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd HHmm");

	    String str2 = readytime1;
	    String str3 = readytime2;
	    long readytime01 = Long.parseLong(str2);
	    long readytime02 = Long.parseLong(str3);
	    readytimeSec = readytime01 * 3600 + readytime02 * 60;

	    try {
	        Date date = sdf.parse(str);
	        time = date.getTime();
	        date.setTime(time);
	        timePercent = time / 1000;
	        System.out.println(timePercent);


	        timer.getDTime(origin_text.getText(), destination_text.getText(), timePercent);
	        drtime = timer.durationTime;
	        timer.setATime(timePercent,readytimeSec,drtime);
	        timer.setDTime(timePercent, drtime);

	        timer.loopsetTime(origin_text.getText(), destination_text.getText(), timePercent);


	        label.setText("Arrival Time:" + arrivaltime1 + ":" + arrivaltime2 + ", Ready Time:" + readytime1 + ":" + readytime2
	        		+ ", " + origin_text.getText() + destination_text.getText() + timePercent + ", " + readytimeSec + ", " + altime
	        		+ ", " + dpttime);

	    	String url = "https://maps.googleapis.com/maps/api/directions/json?"
	        		+ "origin="+ origin_text.getText() +"&destination="+ destination_text.getText() +"&"
	    			+ "arrival_time=" + timePercent
	        		+ "&mode=transit&transit_mode=rail&"
	        		+ "key=AIzaSyAg0eO43cWO7onOVkiEjDPR1vJl6OJ6RNU";

//	        Desktop desktop = Desktop.getDesktop();
//	        desktop.browse(new URI(""+url+""));
	    } catch(Exception e) {
	        e.printStackTrace();
	    }




	}

}
