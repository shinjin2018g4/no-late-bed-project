import java.io.FileInputStream;
import java.util.Date;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.tasks.OnCompleteListener;
import com.google.firebase.tasks.Task;

public class FirebaseSample3
{
  static String JSON_FILE_NAME = "C:\\Users\\ctctime24\\Downloads\\no-late bed project-8ae39642d037.json"; // 認証情報の JSONファイル名のパスを指定。
  static String URL = "https://testv2-77226.firebaseio.com/"; // Firebase上のリアルタイムデータベースのURLを指定。

  public static void firebase() throws Exception
  {
    System.out.println("mainメソッドの処理が開始されました。");

    // 初期化。

    FileInputStream serviceAccount = new FileInputStream(JSON_FILE_NAME);

    FirebaseOptions options = new FirebaseOptions.Builder()
        .setCredentials(GoogleCredentials.fromStream(serviceAccount))
        .setDatabaseUrl(URL)
        .build();

    FirebaseApp.initializeApp(options);

    // Firebase へのデータ送受信を行うサンプル

//    SendingDataToFirebase(0);     // Firebase にデータを送信する
//    sampleReceivingDataFromFirebase1(); // Firebase からデータを受信するサンプル1

    // 上記のデータの送受信が終わるまで、しばらく時間がかかりますので、10秒間待機します。

    try
    {
      Thread.sleep(10000);
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }

    System.out.println("mainメソッドの処理が終了しました。");
  }

  // Firebase にデータを送信するサンプル2

  public static void SendingDataToFirebase(int sign) throws Exception
  {
	  System.out.println("mainメソッドの処理が開始されました。");

	    // 初期化。

	    FileInputStream serviceAccount = new FileInputStream(JSON_FILE_NAME);

	    FirebaseOptions options = new FirebaseOptions.Builder()
	        .setCredentials(GoogleCredentials.fromStream(serviceAccount))
	        .setDatabaseUrl(URL)
	        .build();

	    FirebaseApp.initializeApp(options);
	  
	 DatabaseReference reference = FirebaseDatabase.getInstance().getReference();

    OnCompleteListener listener = new OnCompleteListener()
    {
      @Override
      public void onComplete(Task task)
      {
        System.out.println("送信完了");

      }
    };

    reference.child("message").setValue(sign).addOnCompleteListener(listener);
    reference.child("number").setValue(100).addOnCompleteListener(listener);
    reference.child("folder").child("a").setValue("あああ").addOnCompleteListener(listener);

    reference.child("time").push().setValue("送信時刻 " + new Date()).addOnCompleteListener(listener);
  }

  // Firebase からデータを受信するサンプル1

  public void sampleReceivingDataFromFirebase1()
  {
    DatabaseReference reference = FirebaseDatabase.getInstance().getReference();

    ValueEventListener listener = new ValueEventListener()
    {
      @Override
      public void onDataChange(DataSnapshot snapshot)
      {
        String key   = snapshot.getKey();
        Object value = snapshot.getValue();
        System.out.println("データを受信しました。key=" + key + ", value=" + value);
      }

      @Override
      public void onCancelled(DatabaseError error)
      {
        System.out.println("キャンセルされました。");
      }
    };

    reference.child("key1").addValueEventListener(listener);
    reference.child("key2").addValueEventListener(listener);
    reference.child("key3").addValueEventListener(listener);
  }

}
