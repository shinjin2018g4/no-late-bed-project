import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class Timer03 {


	public static long durationTime;
	public static long durationTime_old;
	public static long arrivalTime;
	public static long readyTime;
	public static long alarmTime;
	public static long deparetureTime;
	public static int stopalarm;

	Firebase04 firebase = new Firebase04();


	public void getDTime(String org, String dst, long tp) throws IOException {

		URL url = new URL( "https://maps.googleapis.com/maps/api/directions/json?"
        		+ "origin="+ org +"&destination="+ dst +"&"
    			+ "arrival_time=" + tp
        		+ "&mode=transit&transit_mode=rail&"
        		+ "key=AIzaSyAg0eO43cWO7onOVkiEjDPR1vJl6OJ6RNU");

		HttpURLConnection httpConn = (HttpURLConnection) url.openConnection();
		InputStream in = httpConn.getInputStream();
		BufferedReader r = new BufferedReader(
		new InputStreamReader(in,"EUC-JP"));

		for (;;) {
			String line = r.readLine();

			if (line.matches(".*duration.*")) {
				line = r.readLine();
				line = r.readLine();
				int idx = line.indexOf(":");
			durationTime = Integer.parseInt(line.substring(idx+2));
			break;
			}
		}

	}

	public static void setATime(long atime, long rtime, long drtime) {

		alarmTime = atime - rtime - drtime;
		System.out.println("setATime");

	}

	public void setDTime(long atime, long drtime) {

		deparetureTime = atime - drtime;
		System.out.println("setDTime");

	}

	public void speakATime() {

		BouyomiChan4J bouyomi = new BouyomiChan4J();

		SimpleDateFormat dfmt = new SimpleDateFormat("h時mm分");
		long ctime = System.currentTimeMillis();
		long drt_m = (durationTime % 3600) / 60;
		long drt_h = durationTime / 3600;

		Date strct = new Date(ctime);
		Date strdptt = new Date(deparetureTime);
		String strdrt;
		if (drt_h == 0) {
			strdrt = String.format("%d",drt_m) + "分" ;
		}else {
			strdrt = String.format("%d",drt_h) + "時間" + String.format("%d",drt_m) + "分" ;
		}

		String spctime = "現在、"+ dfmt.format(strct) + "でございます。";
		String spdpttime = "出発予定時刻は"+ dfmt.format(strdptt) + "でございます。";
		String spdrtime = "移動時間は"+ strdrt + "でございます。";

		bouyomi.talk(spctime);
		bouyomi.talk(spdpttime);
		bouyomi.talk(spdrtime);

	}

	public void speakDTime() {

		BouyomiChan4J bouyomi = new BouyomiChan4J();

		SimpleDateFormat dfmt = new SimpleDateFormat("h時mm分");
		Date strdptt = new Date(deparetureTime);
		String spdpttime = "出発予定時刻、"+ dfmt.format(strdptt) + "になりました。いってらっしゃい。";

		bouyomi.talk(spdpttime);

	}

	public void speakDTimeChange() {

		BouyomiChan4J bouyomi = new BouyomiChan4J();

		SimpleDateFormat dfmt = new SimpleDateFormat("h時mm分");
		Date strdptt = new Date(deparetureTime);
		String spdpttime = "出発予定時刻が"+ dfmt.format(strdptt) + "に変更されました。";

		bouyomi.talk(spdpttime);

	}

	public void loopsetTime(String org, String dst, long tp) {
		try {
			int sign = 0;
			firebase.SendingDataToFirebase(sign);
		} catch (Exception e) {
			e.printStackTrace();
		}
		stopalarm = 0;

		Timer timer = new Timer();
		TimerTask tack = new TimerTask(){

			public void run(){

				try {
					durationTime_old = durationTime;
					getDTime(org,dst,tp);
					long currentTime = System.currentTimeMillis() / 1000;
					System.out.println(currentTime);
					long diff = durationTime_old - durationTime;

					if (Math.abs(diff) > 0){
						if (currentTime<alarmTime) {
							setATime(arrivalTime,readyTime,durationTime);
							setDTime(arrivalTime,durationTime);
						}else {
							setDTime(arrivalTime,durationTime);
							speakDTimeChange();
						}

					}

					System.out.println("Loop");
					System.out.println(alarmTime-currentTime);

					if ((deparetureTime-currentTime)<30){
						System.out.println("DTime");
						System.out.println(deparetureTime-currentTime);
						speakDTime();
						timer.cancel();
					}else if ((alarmTime-currentTime)<60){
						if (stopalarm == 0){
							System.out.println("ATime");
							System.out.println(alarmTime-currentTime);
							int sign = 1;
							firebase.SendingDataToFirebase(sign);
							stopalarm = 100;
							speakATime();
							System.out.println(stopalarm);
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		};
		timer.schedule(tack,1000,1000);


	}

}