#include <CustomStepper.h>

CustomStepper stepper(8, 9, 10, 11, (byte[]){8, B1000, B1100, B0100, B0110, B0010, B0011, B0001, B1001}, 4075.7728395, 12, CW);
boolean rotate1 = true;
boolean rotatedeg = false;
boolean crotate = true;
void setup() {
  // put your setup code here, to run once:
  stepper.setRPM(3);//左の数字は1分間の回転数を表す
  stepper.setSPR(4075.7728395);  //つまり4075ステップで1回転を表す？
}

void loop() {
  // put your main code here, to run repeatedly:
    if (stepper.isDone() &&  rotate1 == false)
  {
    stepper.setDirection(CCW);
    stepper.rotate(2);
    rotate1 = true;
  }
  //ここから回転スタート#############################
  if (stepper.isDone() && rotate1 == true && rotatedeg == false)
  {
    stepper.setDirection(CW);
    stepper.rotateDegrees(90);
    rotatedeg = true;
  }

  //ここから逆回転スタート#############################
  if (stepper.isDone() && rotatedeg == true && crotate == true)
  {
    delay(5000);                       // ベッド傾斜後の待ち時間　1秒＝1000
    stepper.setDirection(CCW);
    stepper.rotateDegrees(90);
    crotate = false;
  }
  stepper.run();


}
