#include <ESP8266WiFi.h>
#include <FirebaseArduino.h>

// Set these to run example.
#define FIREBASE_HOST "testv2-77226.firebaseio.com"
#define FIREBASE_AUTH "8zxkRMVvrtl6Gr9kkMGn96UdxoAHfj2SG7BmG0KF"
#define WIFI_SSID "aterm-c41611-g"
#define WIFI_PASSWORD "9aa3a930ef01c"

void setup() {
  Serial.begin(9600);

  // connect to wifi.
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
  Serial.print("connecting");
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(".");
    delay(500);
  }
  Serial.println();
  Serial.print("connected: ");
  Serial.println(WiFi.localIP());
  
  Firebase.begin(FIREBASE_HOST, FIREBASE_AUTH);
}

int recv_data; // 受信データ
/////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////

void setup() {
  Serial.begin(9600);
  pinMode(13, OUTPUT);
}

void loop() { 

  // 受信バッファに３バイト（ヘッダ＋int）以上のデータが着ているか確認
  if ( Serial.available() >= sizeof('H') + sizeof(int) ) {
    // ヘッダの確認
    if ( Serial.read() == 'H' ) {
      int low = Serial.read(); // 下位バイトの読み取り
      int high = Serial.read(); // 上位バイトの読み取り
      recv_data = makeWord(high,low); // 上位バイトと下位バイトを合体させてint型データを復元
    }
  }

  // 受信したデータに基づいてLEDをON/OFF
  if ( recv_data == 12345 ) {
    digitalWrite(13, HIGH);
  }
  if ( recv_data == -12345 ) {
    digitalWrite(13, LOW);
  }
}
