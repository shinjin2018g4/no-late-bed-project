/*
  DigitalReadSerial

  Reads a digital input on pin 2, prints the result to the Serial Monitor

  This example code is in the public domain.

  http://www.arduino.cc/en/Tutorial/DigitalReadSerial
*/
#include <CustomStepper.h>
CustomStepper stepper(8, 9, 10, 11, (byte[]){8, B1000, B1100, B0100, B0110, B0010, B0011, B0001, B1001}, 4075.7728395, 12, CW);
boolean rotate1 = true;
boolean rotatedeg = false;
boolean crotate = true;
// digital pin 2 has a pushbutton attached to it. Give it a name:
int pushButton = 2;

// the setup routine runs once when you press reset:
///////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////
void setup() {
  // initialize serial communication at 9600 bits per second:
  Serial.begin(9600);
  // make the pushbutton's pin an input:
  pinMode(pushButton, INPUT);

    //sets the RPM
  stepper.setRPM(3);//左の数字は1分間の回転数を表す
  //sets the Steps Per Rotation, in this case it is 64 * the 283712/4455 annoying ger ratio
  //for my motor (it works with float to be able to deal with these non-integer gear ratios)
  stepper.setSPR(4075.7728395);  //つまり4075ステップで1回転を表す？
}

// the loop routine runs over and over again forever:
///////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////
void loop() {
  // read the input pin:
  int buttonState = digitalRead(pushButton);
  // print out the state of the button:
  Serial.println(buttonState);
  
 //when a command is finished it the isDone will return true, it is important to notice that
  //you can't give one command just after another since they don't block the execution, 
  //which allows the program to control multiple motors
  if (stepper.isDone() &&  rotate1 == false)
  {
    //this method sets the direction of rotation, has 3 allowed values (CW, CCW, and STOP) 
    //clockwise and counterclockwise for the first 2
    stepper.setDirection(CCW);
    //this method sets the motor to rotate a given number of times, if you don't specify it, 
    //the motor will rotate untilyou send another command or set the direction to STOP
    stepper.rotate(2);
    rotate1 = true;
  }
  //ここから回転スタート#############################
  if (buttonState ==1 && stepper.isDone() && rotate1 == true && rotatedeg == false)
  {
    stepper.setDirection(CW);
    //this method makes the motor rotate a given number of degrees, it works with float
    //you can give angles like 90.5, but you can't give negative values, it rotates to the direction currently set
    stepper.rotateDegrees(90);
    rotatedeg = true;
  }
 // 
  //ここから逆回転スタート#############################
  if (stepper.isDone() && rotatedeg == true && crotate == true)
  {
    delay(5000);                       // ベッド傾斜後の待ち時間　1秒＝1000
    stepper.setDirection(CCW);
    //this method makes the motor rotate a given number of degrees, it works with float
    //you can give angles like 90.5, but you can't give negative values, it rotates to the direction currently set
    stepper.rotateDegrees(90);
     crotate = false;
  }
  //ここで回転止める#############################
  if (stepper.isDone() && rotatedeg == true && crotate == false)
  {
     stepper.setDirection(STOP);
  }
  //this is very important and must be placed in your loop, it is this that makes the motor steps
  //when necessary
  stepper.run();
}
  

