#include <SoftwareSerial.h>

#define TX_PIN 12 //Arduino pin to ESP8266 TX
#define RX_PIN 13 //Arduino pin to ESP8266 RX
#define SERIAL_WAIT_TIME 5000

SoftwareSerial ESPrSerial ( TX_PIN, RX_PIN);


void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
   ESPrSerial.begin(9600);
   ESPrSerial.setTimeout(SERIAL_WAIT_TIME);

}

void loop() {
  // put your main code here, to run repeatedly:
  String cmd1 ="a";
  String cmd2 ="b";

 sendCommand (cmd1, SERIAL_WAIT_TIME);
 delay(5000);
  
 sendCommand (cmd2,SERIAL_WAIT_TIME);
 delay(5000);
}
bool sendCommand(String cmd, int waitTime) {
  //Serial Command send
  ESPrSerial.print(cmd);
  //Response Check From Serial Buffer

  if ( ESPrSerial.find("OK")) {
    Serial.println ("OK");
    return true;
  } else {
     Serial.println ("NG");
     return false;
  }
}

