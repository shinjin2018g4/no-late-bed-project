#include <CustomStepper.h>

CustomStepper stepper(8, 9, 10, 11, (byte[]){8, B1000, B1100, B0100, B0110, B0010, B0011, B0001, B1001}, 4075.7728395, 12, CW);
boolean rotate1 = false;
boolean rotatedeg = true;
boolean crotate = false;
   
int rpin = 5;
void setup()
{
  Serial.begin(9600);
  //sets the RPM
  stepper.setRPM(3);//左の数字は1分間の回転数を表す
  stepper.setSPR(4075.7728395);  //つまり4075ステップで1回転を表す？
  pinMode(rpin, INPUT);
}

int n = 0;
int old_n = 0;
int up = stepper.isDone();

void loop()
{
  n = digitalRead(rpin);
  Serial.println(n);
  if (n == HIGH && n != old_n){
    Serial.println("Wake up!!");
    rotate1 = true;
    rotatedeg = false;//また動かす
    crotate = true; 
  }

  else{
    Serial.println("Sleep...");
    rotate1 = false;//動かなくする
  }
  //Serial.println("check");

  move();
  //stepper.step(100);
  /*
  Serial.println("before");
  stepper.rotateDegrees(90);
  stepper.run();
  Serial.println("after");
  */
  delay(1000);
  old_n = n;

  //Serial.println("check2");
  //delay(1000*1);
}


void move(){
  Serial.println("check_move");
  /*
  if (stepper.isDone() &&  rotate1 == false)
  {
    Serial.println("move1");
    stepper.setDirection(CCW);
    stepper.rotate(2);
    rotate1 = true;
  }
  Serial.println("move1_end");
  */
//  delay(1000);
  //ここから回転スタート#############################
  if (stepper.isDone() && rotate1 == true && rotatedeg == false)
  {
    Serial.println("move2");
    stepper.setDirection(CW);
    stepper.rotateDegrees(90);
    rotatedeg = true;
  }
  Serial.println("move2_end");
//  delay(1000);
  //ここから逆回転スタート#############################
  if (stepper.isDone() && rotatedeg == true && crotate == true)
  {
    Serial.println("move3");
    delay(5000);                       // ベッド傾斜後の待ち時間　1秒＝1000
    stepper.setDirection(CCW);
    stepper.rotateDegrees(90);
    crotate = false;
    stepper.run();
  }
  //Serial.println("move3_end");
  Serial.println("move_end");
}

