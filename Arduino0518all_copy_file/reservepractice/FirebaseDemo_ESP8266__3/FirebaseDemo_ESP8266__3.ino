//
// Copyright 2015 Google Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

// FirebaseDemo_ESP8266 is a sample that demo the different functions
// of the FirebaseArduino API.

#include <ESP8266WiFi.h>
#include <FirebaseArduino.h>

// Set these to run example.
#define FIREBASE_HOST "testv2-77226.firebaseio.com"
#define FIREBASE_AUTH "8zxkRMVvrtl6Gr9kkMGn96UdxoAHfj2SG7BmG0KF"
#define WIFI_SSID "aterm-c41611-g"
#define WIFI_PASSWORD "9aa3a930ef01c"

/////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////

void setup() {
  Serial.begin(9600);

  // connect to wifi.
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
  Serial.print("connecting");
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(".");
    delay(500);
  }
  Serial.println();
  Serial.print("connected: ");
  Serial.println(WiFi.localIP());
  
  Firebase.begin(FIREBASE_HOST, FIREBASE_AUTH);

  // get value 
  Serial.print("number: ");
  Serial.println(Firebase.getFloat("number"));
  delay(1000);

if (Firebase.getFloat("number")) {

#include <CustomStepper.h>

CustomStepper stepper(8, 9, 10, 11, (byte[]){8, B1000, B1100, B0100, B0110, B0010, B0011, B0001, B1001}, 4075.7728395, 12, CW);
boolean rotate1 = true;
boolean rotatedeg = false;
boolean crotate = true;

 //sets the RPM
  stepper.setRPM(3);//左の数字は1分間の回転数を表す
  //sets the Steps Per Rotation, in this case it is 64 * the 283712/4455 annoying ger ratio
  //for my motor (it works with float to be able to deal with these non-integer gear ratios)
  stepper.setSPR(4075.7728395);  //つまり4075ステップで1回転を表す？

}
int n = 0;

//////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////

void loop()
{
  //when a command is finished it the isDone will return true, it is important to notice that
  //you can't give one command just after another since they don't block the execution, 
  //which allows the program to control multiple motors
  if (stepper.isDone() &&  rotate1 == false)
  {
    //this method sets the direction of rotation, has 3 allowed values (CW, CCW, and STOP) 
    //clockwise and counterclockwise for the first 2
    stepper.setDirection(CCW);
    //this method sets the motor to rotate a given number of times, if you don't specify it, 
    //the motor will rotate untilyou send another command or set the direction to STOP
    stepper.rotate(2);
    rotate1 = true;
  }
  //ここから回転スタート#############################
  if (stepper.isDone() && rotate1 == true && rotatedeg == false)
  {
    stepper.setDirection(CW);
    //this method makes the motor rotate a given number of degrees, it works with float
    //you can give angles like 90.5, but you can't give negative values, it rotates to the direction currently set
    stepper.rotateDegrees(90);
    rotatedeg = true;
  }
 // 
  //ここから逆回転スタート#############################
  if (stepper.isDone() && rotatedeg == true && crotate == true)
  {
    delay(5000);                       // ベッド傾斜後の待ち時間　1秒＝1000
    stepper.setDirection(CCW);
    //this method makes the motor rotate a given number of degrees, it works with float
    //you can give angles like 90.5, but you can't give negative values, it rotates to the direction currently set
    stepper.rotateDegrees(90);
     crotate = false;
  }
  //ここで回転止める#############################
  if (stepper.isDone() && rotatedeg == true && crotate == false)
  {
     stepper.setDirection(STOP);
  }
  //this is very important and must be placed in your loop, it is this that makes the motor steps
  //when necessary
  stepper.run();
}


  

 
}
