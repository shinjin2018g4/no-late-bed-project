extern "C" {
#include <ESP8266WiFi.h>   // A/D (system_adc_read) 用
}

#include <Ticker.h>

Ticker ticker;
Ticker moter;

#define SCK 14                // CLK       クロック信号出力ピン
#define RCK 12                // LOAD      ラッチ動作出力ピン
#define SI  13                // MOSI - DI データ信号出力ピン
#define CLR 15                // クリア

// 2相励磁 (4096ステップで2回転)
const int S = 4;
const int steps[S] = {
//  HGFEDCBA
  0b00001100,
  0b00000110,
  0b00000011,
  0b00001001
};

// 初期化
void setup() {
  Serial.begin(115200);
  Serial.println("");

  pinMode(SCK, OUTPUT) ;
  pinMode(RCK, OUTPUT) ;
  pinMode(SI,  OUTPUT) ;
  pinMode(CLR, OUTPUT) ;

  // 温度センサー
  // 1,000 msec 毎に取得
  ticker.attach_ms(1000, timer);

  // 回転速度調整
  // 回転速度遅すぎる(1/64)のでもっと高速に回転させたいが
  // 2 msec だと脱調することがある
  // 3 msec が限界だった
  moter.attach_ms(3, drive);
}

// ループ
void loop() {
  // Do nothing.
}

// シリアル出力
void out(int x) {
  digitalWrite(CLR, LOW); // クリア信号
  digitalWrite(CLR, HIGH);

  digitalWrite(RCK, LOW);
  shiftOut(SI, SCK, MSBFIRST, x);
  digitalWrite(RCK, HIGH); // ラッチ信号
}

const int RANGE = 1024;

volatile int P_ref = 0; // 目標位置
volatile int P_now = 0; // 現在位置

// A/Dセンサー値取得
void timer() {

  // A/Dセンサー値取得
  int tout = system_adc_read();
  Serial.println(tout);

  // A/Dセンサーの値(tout)を、-RANGE〜RANGE にマップする
  P_ref = map(tout, 0, 1024, -RANGE, RANGE);
}

// モータードライブ
void drive() {
  // 目標位置 と 現在位置 との 差
  int d = P_ref - P_now;

  if (d != 0) {

    // P_ref に近づくように ±1 する
    P_now += d / abs(d);

    Serial.print(" P_ref:");
    Serial.print(P_ref);
    Serial.print(" P_now:");
    Serial.print(P_now);

    // P_now にするには、どの方向に回せばよいか
    int i = (P_now + RANGE) % S;

    Serial.print( " i:");
    Serial.print(i);
    Serial.println("");

    // 1ステップ移動する
    out(steps[i]);

  } else {
    // 0を出力すれば電流が流れない
    out(0);
  }
}
