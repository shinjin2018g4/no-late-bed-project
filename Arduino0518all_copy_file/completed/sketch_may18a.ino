#include <CustomStepper.h>
 
CustomStepper stepper(8, 9, 10, 11, (byte[]){8, B1000, B1100, B0100, B0110, B0010, B0011, B0001, B1001}, 4075.7728395, 12, CW);
boolean rotate1 = true;
boolean rotatedeg = false;
boolean crotate = true;

 ////////////////////////////////////////////////////////////////////////////
int rpin = 5;
void setup()
{
  Serial.begin(9600);
  //sets the RPM
  stepper.setRPM(3);//左の数字は1分間の回転数を表す
  stepper.setSPR(4075.7728395);  //つまり4075ステップで1回転を表す？
  pinMode(rpin, INPUT);
}

 ///////////////////////////////////////////////////////////////////////////
int n = 0;
void loop()
{
  while(1){
  n = digitalRead(rpin);  
  //Serial.println(n);
  if (n == LOW){
    Serial.println("sleep");
    continue;
  }
  else{
  Serial.println("wake up");
  //ここから回転スタート#############################
  if (stepper.isDone() && rotate1 == true && rotatedeg == false)
  {
    Serial.println("move2");
    stepper.setDirection(CW);
    stepper.rotateDegrees(90);
    rotatedeg = true;
  }
 // 
  //ここから逆回転スタート#############################
  if (stepper.isDone() && rotatedeg == true && crotate == true)
  {
    Serial.println("move3");
    delay(5000);                       // ベッド傾斜後の待ち時間　1秒＝1000
    stepper.setDirection(CCW);
    stepper.rotateDegrees(90);
    crotate = false;
  }

  stepper.run();
  }

  }
}
