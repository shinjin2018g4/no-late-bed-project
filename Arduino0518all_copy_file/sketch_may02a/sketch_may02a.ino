const int inPin = 2;
int input = 0;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(inPin, INPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  input = digitalRead(inPin);
  if(input == HIGH){
    Serial.println ("HIGH");
  }else{
    Serial.println("LOW");
  }
delay(1000);
}
